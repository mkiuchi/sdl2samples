﻿using CommandLine;
using SkiaSharp;
using SDL2;
using System.Runtime.InteropServices;
using System.Numerics;

class Ball
{
    /*
        ボールのオブジェクトクラス
    */
    public int x, y;
    public int SCREEN_X, SCREEN_Y;
    public Vector2 pos, direction, invertx, inverty;
    public float speed;

    public Ball(Vector2 pos, Vector2 direction, float speed, int SCREEN_X, int SCREEN_Y)
    {
        // 初期化
        // pos=初期位置, direction=進行方向, speed=スピード
        // SCREEN_X, SCREEN_Y=targetの縦横の大きさ
        this.pos = pos;
        this.x = (int)pos[0];
        this.y = (int)pos[1];
        this.direction = direction;
        this.invertx = new Vector2(new float[] {(float)-1.0, (float)1.0});
        this.inverty = new Vector2(new float[] {(float)1.0, (float)-1.0});
        this.speed = speed;
        this.SCREEN_X = SCREEN_X;
        this.SCREEN_Y = SCREEN_Y;
    }

    public void Move()
    {
        // 進行方向、スピードを足して位置をアップデート
        pos = pos + Vector2.Multiply(direction, (float)speed);
        x = (int)pos[0];
        y = (int)pos[1];
        // ボールが画面外に出たら方向を反転
        if (x<0 || x>SCREEN_X)
        {
            direction *= invertx;
        }
        if (y<0 || y>SCREEN_Y)
        {
            direction *= inverty;
        }
    }
}

class Renderer
{
    /*
       レンダラ
       ソフトウェアレンダリングとハードウェアレンダリングを抽象化している
    */
    static private IntPtr selfwindow;
    static private IntPtr windowSurfacePtr;
    static private SDL.SDL_Surface windowSurface;
    public IntPtr hwrenderer;
    public string method;

    public Renderer(string method, IntPtr window)
    {
        this.method = method;
        selfwindow = window;
        if (method == "software")
        {
            // ソフトウェアレンダリングの場合はtarget surfaceを取得
            windowSurfacePtr = SDL.SDL_GetWindowSurface(selfwindow);
            windowSurface = Marshal.PtrToStructure<SDL.SDL_Surface>(windowSurfacePtr);
        }
        else
        {
            // ハードウェアレンダリングの場合はレンダラを生成
            hwrenderer = SDL.SDL_CreateRenderer(
                window, 
                -1, 
                SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED | 
                SDL.SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC);
        }
    }
    public void Render(List<Ball> components, IntPtr texture)
    {
        if (method == "software")
        {
            // ソフトウェアレンダリングの場合
            // 全画面を黒でクリア
            var background = new SDL.SDL_Rect {
                x = 0, y = 0,
                w = windowSurface.w, h = windowSurface.h };
            _ = SDL.SDL_FillRect(
                    windowSurfacePtr,
                    ref background,
                    SDL.SDL_MapRGB(windowSurface.format, 0, 0, 0)
                );
            // ボールのsurfaceをSDL_BlitSurfaceでtargetに描画する
            var r = new SDL.SDL_Rect { x = 0, y = 0, w = 0, h = 0 };
            foreach (var c in components)
            {
                r.x = c.x;
                r.y = c.y;
                _ = SDL.SDL_BlitSurface(texture, (nint)null, windowSurfacePtr, ref r);
            }
            // target surfaceをアップデート
            _ = SDL.SDL_UpdateWindowSurface(selfwindow);
        }
        else
        {
            // ハードウェアレンダリングの場合
            // 全画面を黒でクリア
            if (SDL.SDL_SetRenderDrawColor(hwrenderer, 0,0,0,255) < 0)
            {
                Console.WriteLine($"SDL_SetRenderDrawColor failed {SDL.SDL_GetError()}");
            }
            if (SDL.SDL_RenderClear(hwrenderer) < 0)
            {
                Console.WriteLine($"SDL_RenderClear failed {SDL.SDL_GetError()}");
            }
            // ボールのtextureをSDL_RenderCopyでtargetに描画
            _ = SDL.SDL_QueryTexture(texture, out uint format, out int access, out int w, out int h);
            var r = new SDL.SDL_Rect { x = 0, y = 0, w = w, h = h };
            foreach(var c in components)
            {
                r.x = c.x;
                r.y = c.y;
                if (SDL.SDL_RenderCopy(hwrenderer, texture, (nint)null, ref r) <0)
                {
                    Console.WriteLine($"SDL_RenderCopy failed {SDL.SDL_GetError()}");
                }
            }
            // targetをアップデート
            SDL.SDL_RenderPresent(hwrenderer);
        }
        
    }

}
class Program
{
    static readonly string TITLE = "Ball";
    static readonly int SCREEN_X = 640;
    static readonly int SCREEN_Y = 480;

    static int CheckEvent()
    {
        /*
            キーボードイベントに応じて対応を分ける
            return code:
                -1 : 終了 (Ctrl+C, Q, ESC)
                1 : 一時停止 (P)
                0 : なにもしない
        */
        while (SDL.SDL_PollEvent(out SDL.SDL_Event e) != 0)
        {
            switch (e.type)
            {
                case SDL.SDL_EventType.SDL_QUIT:
                    // ウィンドウを閉じるなど、SDL_QUITシグナルが出たら終了
                    return -1;
                case SDL.SDL_EventType.SDL_KEYDOWN:
                    var scancode = e.key.keysym.scancode;
                    var keymod = e.key.keysym.mod;
                    // P キーを押したらポーズ
                    if (scancode == SDL.SDL_Scancode.SDL_SCANCODE_P)
                    {
                        return 1;
                    }
                    // ESC=>終了
                    if (scancode == SDL.SDL_Scancode.SDL_SCANCODE_ESCAPE)
                    {
                        return -1;
                    }
                    // Q=>終了
                    if (scancode == SDL.SDL_Scancode.SDL_SCANCODE_Q)
                    {
                        return -1;
                    }
                    // Ctrl+C=>終了
                    if (scancode == SDL.SDL_Scancode.SDL_SCANCODE_C)
                    {
                        if (keymod == SDL.SDL_Keymod.KMOD_LCTRL || keymod == SDL.SDL_Keymod.KMOD_RCTRL)
                        {
                            return -1;
                        }
                    }
                    break;
            }
        }
        return 0;
    }

    static IntPtr LoadImage(string type, Renderer renderer, string path, int DstWidth, int DstHeight)
    {
        /*
           画像を読み込み、
           ソフトウェアレンダラであればsurface、
           ハードウェアレンダラであればtextureを返す。
           Skiaを使っているのはリサイズ後の画像をスムーズにするため
        */
        // Skiaで画像を読み込みリサイズ
        var resized = SKBitmap.Decode(path).Resize(
            new SKImageInfo(DstWidth, DstHeight),
            SKFilterQuality.Medium);
        if (resized.GetPixels() == IntPtr.Zero)
        {
            Console.WriteLine("GetPixels has no pixel buffer");
            return IntPtr.Zero;
        }
        // Pythonのsdl2.ext.imageを参考にマスクをセットしている
        // 参考: https://pysdl2.readthedocs.io/en/stable/_modules/sdl2/ext/image.html#pillow_to_surface
        uint rmask, gmask, bmask, amask;
        if (BitConverter.IsLittleEndian)
        {
            rmask = (uint)Convert.ToInt32("0x000000ff", 16);
            gmask = (uint)Convert.ToInt32("0x0000ff00", 16);
            bmask = (uint)Convert.ToInt32("0x00ff0000", 16);
            amask = (uint)Convert.ToInt32("0xff000000", 16);
        }
        else
        {
            rmask = (uint)Convert.ToInt32("0xff000000", 16);
            gmask = (uint)Convert.ToInt32("0x00ff0000", 16);
            bmask = (uint)Convert.ToInt32("0x0000ff00", 16);
            amask = (uint)Convert.ToInt32("0x000000ff", 16);
        }
        // surfaceを作成
        var spriteSurface = SDL.SDL_CreateRGBSurfaceFrom(
            resized.GetPixels(),
            DstWidth, DstHeight,
            32,
            4*DstWidth,
            rmask, gmask, bmask, amask
        );
        if (spriteSurface == IntPtr.Zero)
        {
            Console.WriteLine("SDL_CreateRGBSurfaceFrom failed");
            return IntPtr.Zero;
        }
        if (type == "hardware")
        {
            // ハードウェアレンダリングの場合はsurfaceからtextureを生成
            IntPtr texture =  SDL.SDL_CreateTextureFromSurface(renderer.hwrenderer, spriteSurface);
            if (texture == IntPtr.Zero)
            {
                Console.WriteLine($"SDL_CreateTextureFromSurface failed {SDL.SDL_GetError()}");
                return IntPtr.Zero;
            }
            return texture;
        }
        return spriteSurface;
    }

    static List<Ball> CreateBalls(int numballs)
    {
        /*
          指定された個数分ボールを作成する
        */
        var r = new System.Random();
        int cnt = 0;
        List<Ball> retlist = new List<Ball>();
        while (true)
        {
            var x = (float)r.Next((SCREEN_X/2)-20, (SCREEN_X/2)+20);
            var y = (float)r.Next((SCREEN_Y/2)-20, (SCREEN_Y/2)+20);
            var dx = (float)r.NextDouble() * 2 - 1;
            var dy = (float)r.NextDouble() * 2 - 1;
            var speed = (float)(r.NextDouble() + (r.NextDouble() + 5));
            if (x<0 || x>SCREEN_X)
            {
                continue;
            }
            if (y<0 || y> SCREEN_Y)
            {
                continue;
            }
            var newBall = new Ball(
                    new Vector2(new float[] {x, y}),
                    new Vector2(new float[] {dx, dy}),
                    speed,
                    SCREEN_X, SCREEN_Y);

            retlist.Add(newBall);
            cnt++;
            if (cnt >= numballs)
            {
                break;
            }
        }
        return retlist;
    }
    static int Run(string renderflg, int numballs)
    {
        /*
            SDLのメインループ
        */
        var pause = false;
        // SDL の初期化
        if (SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING) < 0)
        {
            Console.WriteLine($"SDL init failed. {SDL.SDL_GetError()}");
        }

        // ウィンドウとレンダラの初期化
        IntPtr window;
        Renderer spriteRenderer;
        if(renderflg == "software")
        {
            window = SDL.SDL_CreateWindow(
                TITLE,
                SDL.SDL_WINDOWPOS_UNDEFINED, SDL.SDL_WINDOWPOS_UNDEFINED,
                SCREEN_X, SCREEN_Y,
                SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
            spriteRenderer = new Renderer("software", window);
        }
        else
        {
            window = SDL.SDL_CreateWindow(
                TITLE,
                SDL.SDL_WINDOWPOS_UNDEFINED, SDL.SDL_WINDOWPOS_UNDEFINED,
                SCREEN_X, SCREEN_Y,
                SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN|SDL.SDL_WindowFlags.SDL_WINDOW_OPENGL);
            spriteRenderer = new Renderer("hardware", window);
        }
        if (window == IntPtr.Zero)
        {
            Console.WriteLine($"Window init failed {SDL.SDL_GetError()}");
        }
        
        // ボールのスプライト画像を作成
        var texture_ball = LoadImage(
            renderflg,
            spriteRenderer,
            "resources/circle_FILL1_wght400_GRAD0_opsz48.png",
            12, 12);
        if (texture_ball == IntPtr.Zero)
        {
            Console.WriteLine($"texture_ball load failed {SDL.SDL_GetError()}");
        }

        // 指定された個数分ボールを作成
        List<Ball> balls = CreateBalls(numballs);

        // メインループ
        var running = true;
        while (running)
        {
            // イベントの処理(一時停止、終了)
            var e = CheckEvent();
            if (e == -1)
            {
                running = false;
            }
            if (e == 1)
            {
                pause = !pause;
            }

            foreach (Ball ball in balls)
            {
                if (!pause)
                {
                    // ボールの移動処理
                    ball.Move();
                }
            }

            // ボールのレンダリング
            spriteRenderer.Render(balls, texture_ball);

            // おおよそ60FPSになるよう15msのディレイを入れる
            SDL.SDL_Delay(15);
        }

        // メインループを抜けたらSDLの終了処理
        SDL.SDL_DestroyRenderer(spriteRenderer.hwrenderer);
        SDL.SDL_DestroyWindow(window);
        SDL.SDL_Quit();

        return 0;
    }
    static void Main(string[] args)
    {
        /*
            オプションのパース
        */
        var parsedResult = Parser.Default.ParseArguments<Options>(args);
        Options opt;
        var rendering = "";
        int balls = 0;

        switch (parsedResult.Tag)
        {
            case ParserResultType.Parsed:
                // parseが成功したらrenderingオプションを取り出し
                if (parsedResult is not Parsed<Options> parsed)
                {
                    break;
                }
                opt = parsed.Value;
                // renderingオプションをセット
                Console.WriteLine($"Rendering method: {opt.Rendering}");
                if (opt.Rendering != null)
                {
                    rendering = opt.Rendering;
                }
                // ballsオプションをセット
                balls = opt.Balls;
                Console.WriteLine($"number of balls: {balls}");
                // remainingオプションは未使用
                string? strRemaining;
                if (opt.Remaining is not null)
                {
                    strRemaining = string.Concat("{ ", string.Join(", ", opt.Remaining.Select(e => $"\"{e}\"")), " }");
                    // Console.WriteLine($"Remaining arguments{strRemaining}");
                }
                break;
            default:
                // parseが失敗したらエラーで終了
                Console.WriteLine("specify rendering option");
                Environment.Exit(1);
                break;
        }

        if (Program.Run(rendering, balls) == 0)
        {
            Environment.Exit(0);
        }
    }
}
