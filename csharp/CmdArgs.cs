using CommandLine;

class Options
{
    // レンダリングオプション
    [Option('r', "rendering",
        Required = false,
        HelpText = "rendering method (\"software\" or \"hardware\")",
        Default = "software")]
    public string? Rendering { get; set; }
    // ボールの個数
    [Option('b', "balls",
        Required = false,
        HelpText ="number of balls (integer)",
        Default = 8)]
    public int Balls { get; set; }
    // 残りのオプション(無視する)
    [Value(1, MetaName = "remaining", Required = false)]
    public IEnumerable<string>? Remaining { get; set; }
}