# setup

- `apt install dotnet-sdk-7.0`
- change directory to csharp
- `dotnet add package CommandLineParser`
- `dotnet add package SkiaSharp SkiaSharp.NativeAssets.Linux`
- `dotnet add package SDL2-CS.NetCore`
- `cd csharp/bin/Debug/net7.0; ln -s /usr/lib/x86_64-linux-gnu/libSDL2-2.0.so.0 ./SDL2.dll.so`
- `cd csharp/bin/Debug/net7.0; ln -s /usr/lib/x86_64-linux-gnu/libSDL2_image-2.0.so.0 ./SDL2_image.dll.so`

# create new SDL project

- `apt install dotnet-sdk-7.0`
- create directory(csharp), change directory to csharp
- `dotnet new console`
- `dotnet new gitignore`
- `dotnet add package SDL2-CS.NetCore`
- `cd csharp/bin/Debug/net7.0; ln -s /usr/lib/x86_64-linux-gnu/libSDL2-2.0.so.0 ./SDL2.dll.so`
- `cd csharp/bin/Debug/net7.0; ln -s /usr/lib/x86_64-linux-gnu/libSDL2_image-2.0.so.0 ./SDL2_image.dll.so`

# REPL Tool

- dotnet-repl
    - `dotnet tool install -g dotnet-repl`

# ref

- jp
    - https://www.curict.com/item/3d/3dac60a.html
- en
    - https://jsayers.dev/tutorials/
    - https://www.freepascal-meets-sdl.net/chapter-4-imagestextures/
    - https://weblogs.asp.net/bleroy/resizing-images-from-the-server-using-wpf-wic-instead-of-gdi
    - https://stackoverflow.com/questions/8553136/system-drawing-namespace-not-found-under-console-application
    - https://devblogs.microsoft.com/dotnet/net-core-image-processing/
    - 

