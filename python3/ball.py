import argparse
import ctypes
import numpy as np
import os
from PIL import Image
import random
import sdl2
import sdl2.ext
import datetime
import sys

TITLE = "Balls"
FLAGS = {
    "software": {
        "window": (sdl2.SDL_WINDOW_SHOWN),
    },
    "hardware": {
        "window": (sdl2.SDL_WINDOW_OPENGL|sdl2.SDL_WINDOW_SHOWN),
    }
}
ARGS = None
SCREEN_X=640
SCREEN_Y=480
SPRITE_SIZE=16

class Ball:
    #
    # ボールのオブジェクトクラス
    #
    def __init__(self, start, direction, speed):
        #
        # ボールの初期化
        # start=初期位置, direction=進行方向, speed=スピード
        # 
        self.x = start[0]
        self.y = start[1]
        self.direction = np.array(direction)
        self.speed = speed
    def move(self):
        #
        # ボールの移動
        #
        # 進行方向、スピードを足して位置をアップデート
        currentpos = np.array([self.x, self.y])
        nextpos = currentpos + self.direction*self.speed
        self.x = int(nextpos[0])
        self.y = int(nextpos[1])
        # ボールが画面外に出たら方向を反転
        if nextpos[0] < 0 or nextpos[0] > SCREEN_X:
            self.direction = self.direction * np.array([-1,1])
        if nextpos[1] < 0 or nextpos[1] > SCREEN_Y:
            self.direction = self.direction * np.array([1,-1])

class SoftwareRenderSystem(sdl2.ext.SoftwareSpriteRenderSystem):
    #
    # ソフトウェアレンダラ
    #
    def __init__(self, window):
        super(SoftwareRenderSystem, self).__init__(window)
        print("using software rendering")
    def render(self, components, texture_ball):
        # 全画面を黒でクリア
        sdl2.ext.fill(self.surface, sdl2.ext.Color(0,0,0))
        # ボールのsurfaceをSDL_BlitSurfaceでtargetに描画する
        r = sdl2.SDL_Rect(0,0,0,0)
        for c in components:
            r.x = c.x
            r.y = c.y
            sdl2.SDL_BlitSurface(
                texture_ball,
                None,
                self.surface,
                r
            )
        # targetをアップデート
        sdl2.SDL_UpdateWindowSurface(self.window)

class HardwareRenderSystem(sdl2.ext.TextureSpriteRenderSystem):
    #
    # ハードウェアレンダラ
    #
    def __init__(self, renderer):
        super(HardwareRenderSystem, self).__init__(renderer)
        currentDriverInfoRet = ctypes.create_string_buffer(b'test', size=64)
        currentDriverInfoRet = sdl2.SDL_GetCurrentVideoDriver()
        print("Using hardware acceleration. Driver:", currentDriverInfoRet.decode('utf-8'))
    def render(self, components, texture_ball):
        renderer = self.sdlrenderer
        # 全画面を黒でクリア
        sdl2.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255)
        sdl2.SDL_RenderClear(renderer)
        # ボールのスプライトサイズを取得
        w = ctypes.c_int()
        h = ctypes.c_int()
        sdl2.SDL_QueryTexture(texture_ball, None, None, w, h)
        # ボールのtextureをSDL_RenderCopyでtargetに描画する
        r = sdl2.SDL_Rect(0,0,0,0)
        for c in components:
            r.x = c.x or 0
            r.y = c.y or 0
            r.w = w
            r.h = h
            if sdl2.SDL_RenderCopy(
                renderer,
                texture_ball,
                None,
                r
            ):
                raise sdl2.SDL_Error
        # targetをアップデート
        sdl2.SDL_RenderPresent(renderer)

def check_event(events):
    for event in events:
        # 終了処理(return code -1)
        #  - ウィンドウを閉じる
        #  - Q, ESC, Ctrl+C を押す
        # 一時停止(return code 1)
        #  - P キーを押す
        if event.type == sdl2.SDL_QUIT:
            return -1
        if event.type == sdl2.SDL_KEYDOWN:
            # P key => Pause
            if event.key.keysym.scancode == sdl2.SDL_SCANCODE_P:
                return 1
            # ESC key => Quit
            if event.key.keysym.scancode == sdl2.SDL_SCANCODE_ESCAPE:
                return -1
            # Ctrl+C => Quit
            if (event.key.keysym.scancode == sdl2.SDL_SCANCODE_C) and (event.key.keysym.mod & sdl2.KMOD_CTRL):
                return -1
            # Q key => Quit
            if event.key.keysym.scancode == sdl2.SDL_SCANCODE_Q:
                return -1
    return 0

def load_image(type, renderer, path):
    #
    # 画像を読み込み、
    # ソフトウェアレンダラであればsurface、
    # ハードウェアレンダラであればtextureを返す。
    #
    image = Image.open(path).convert(mode="RGBA").resize((SPRITE_SIZE, SPRITE_SIZE), resample=Image.Resampling.LANCZOS)
    surface = sdl2.ext.image.pillow_to_surface(image, as_argb=True)
    if type == "hardware":
        hwrenderer = renderer.sdlrenderer
        return sdl2.SDL_CreateTextureFromSurface(hwrenderer, surface)
    else:
        return surface


def create_balls(numballs):
    #
    # 指定された個数分ボールを生成する
    #
    random.seed(datetime.datetime.now().second)
    cnt = 0
    retlist = []
    while True:
        x = random.randint(int((SCREEN_X/2) - 20), int((SCREEN_X/2) + 20))
        y = random.randint(int((SCREEN_Y/2) - 20), int((SCREEN_Y/2) + 20))
        dx = random.random() * 2 - 1
        dy = random.random() * 2 - 1
        speed = random.random() + (random.random()+5)
        if x < 0 or x > SCREEN_X:
            continue
        if y < 0 or y > SCREEN_Y:
            continue
        retlist.append(Ball((x, y), (dx, dy), speed))
        cnt += 1
        if cnt >= numballs:
            break
    return retlist

def run():
    #
    # SDL のメインループ
    #
    pause = False
    # SDLの初期化
    sdl2.ext.init()
    # ウィンドウとレンダラの初期化
    if ARGS.rendering == "hardware":
        window = sdl2.ext.Window(
            TITLE,
            size=(SCREEN_X, SCREEN_Y),
            flags=(sdl2.SDL_WINDOW_OPENGL|sdl2.SDL_WINDOW_SHOWN))
        renderer = sdl2.ext.Renderer(window, flags=sdl2.SDL_RENDERER_ACCELERATED)
        factory = sdl2.ext.SpriteFactory(sdl2.ext.TEXTURE, renderer=renderer)
        spriterenderer = HardwareRenderSystem(renderer)
    else:
        window = sdl2.ext.Window(
            TITLE,
            size=(SCREEN_X, SCREEN_Y),
            flags=(sdl2.SDL_WINDOW_SHOWN))
        factory = sdl2.ext.SpriteFactory(sdl2.ext.SOFTWARE)
        renderer = None
        spriterenderer = SoftwareRenderSystem(window)
    window.show()

    # ボールのスプライト画像を作成
    basedir = ""
    if os.path.dirname(sys.argv[0]) != "":
        basedir = os.path.dirname(sys.argv[0]) + "/"
    texture_ball = load_image(ARGS.rendering,
                              renderer,
                              basedir + "resources/circle_FILL1_wght400_GRAD0_opsz48.png")

    # 指定された個数分ボールを生成
    balls = create_balls(int(ARGS.balls))

    # メインループ
    pause = False
    while True:
        # イベントの処理(一時停止、終了)
        e = check_event(sdl2.ext.get_events())
        if e == -1:
            # 終了
            break
        if e == 1:
            # 一時停止
            pause = not pause
        if not pause:
            # ボールの移動処理
            for b in balls:
                b.move()
        # ボールのレンダリング
        spriterenderer.render(balls, texture_ball)
        # おおよそ60FPSになるよう15msのディレイを入れる
        sdl2.SDL_Delay(15)

if __name__ == "__main__":
    #
    # オプションのパース
    #
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-r', '--rendering',
                           default='software',
                           required=False,
                           choices=["software", "hardware"],
                           help="rendering method")
    argparser.add_argument('-b', '--balls',
                           default=8,
                           required=False,
                           help="number of balls")
    ARGS = argparser.parse_args()
    sys.exit(run())
